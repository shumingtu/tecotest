import { Injectable } from '@angular/core';
import { DexieProvider } from '../providers/dexieProvider';
import { OrderListDao } from './dao/orderListDao';
import { MessagesDao } from './dao/messagesDao';

@Injectable()
export class DBManager {
    orderList: any;
    messages: any;

    constructor (
        public orderListDao: OrderListDao,
        public messagesDao: MessagesDao) {

    }

    async init() {
        console.log('db init start');

        try {
            await DexieProvider.getDB().open();
            await Promise.all([
                this.orderListDao.init(),
                this.messagesDao.init(),
            ]);
                
            this.orderList = this.orderListDao;
            this.messages = this.messagesDao;
            console.log('DB init OK');
        } catch (error) {
            console.log('DB init error: ' + error);
        }
    }
}
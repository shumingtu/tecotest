import { Injectable } from '@angular/core';
import { DexieProvider } from '../../providers/dexieProvider';

import * as _ from "lodash";

@Injectable()
export class MessagesDao {
    messages = [];
    table: any = DexieProvider.getDB().messages;

    constructor() {
    }

    async init() {
        try {
            let counter = 0;
            let collection = await this.table.toCollection();
            let count = await collection.count();

            if (count !== 0) {
                await collection.each((item) => {
                    this.messages.push(item);
                })
            }
            console.log('init OK');
        } catch (err) {
            console.error(err);
            throw new Error(err);
        }
    }

    async addWithPromise(item) {
        try {
            await this.table.put(item);
            this.messages.push(item);
        } catch (err) {
            console.error(err);
            throw new Error(err);
        }
    }

    async putWithPromise(item) {
        try {
            let index = await _.findIndex(this.messages, function (order) {
                return order.id == item.id;
            });
            console.log(index);

            if (index < 0) {
                //console.log('invalid item');
                throw new Error('invalid item: ' + index);
            }

            await this.table.put(item);
            console.log('put OK');
            this.messages[index] = item;
        } catch (err) {
            console.log(err);
            throw new Error(err);
        }
    }

    async addBulkWithPromise(items) {
        await this.table.bulkPut(items);
    }

    async getById(id) {
        let data = await this.table.get(id);

        return data;
    }

    list() {
        console.log(this.messages);
        return this.messages;
    }

    async deleteBulkWithPromise(ids) {
        let data = await this.table.bulkDelete(ids);

        return data;
    }

    async clear() {
        await this.table.clear();
    }

    async count() {
        let count = await this.table.count();

        return count;
    }
}
import { Injectable } from '@angular/core';
import * as _ from "lodash";

@Injectable()
export class CartManager {
    cart = {
        items: [],
        //location: null,
        //date: null,
        //time: null,
        //note: null,
    };

    list = () => {
        return this.cart.items;
    }

    get = (key) => {
        return this.cart[key];
    }

    add = (item) => {
        //this.cart.items.push(item);
        this.cart.items.push(Object.assign({}, item));
        //this.cart.items = _.sortBy(this.cart.items, 'model');
    }

    set = (key, value) => {
        this.cart[key] = value;
    }

    setOrder = (order) => {
        this.cart = order;
    }

    clear = () => {
        this.cart = {
            items: [],
            //location: null,
            //date: null,
            //time: null,
            //note: null,
        };
    }

    /*remove(key){
        delete this.cart[key];
    }*/

    remove(item) {
        _.remove(this.cart.items, { PRODUCTCODE: item.PRODUCTCODE });
    }
}
import { Injectable } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { FCM } from '@ionic-native/fcm';
import { CartManager } from './cartManager';
//import { UserProvider } from '../provider/userProvider';
//import { SpinnerProvider } from '../provider/spinnerProvider';
import { AlertProvider } from '../providers/alertProvider';
import { Constant } from '../app/constant';
import { Api } from '../services/api';
//import { CartManager } from './cartManager';

declare let chcp: any;
declare let cordova: any;

@Injectable()
export class CordovaService {

    isRequestAppUpdate = false;

    constructor(
        public platform: Platform,
        public network: Network,
        public alertProvider: AlertProvider,
        public splashScreen: SplashScreen,
        //public spinner: SpinnerProvider,
        //public userProvider: UserProvider,
        public device: Device,
        public uniqueDeviceID: UniqueDeviceID,
        public appVersion: AppVersion,
        public api: Api,
        public fcm: FCM,
        public cartManager: CartManager
        ) { }

    init() {
        this.network.onConnect().subscribe(this.onOnline)
        this.network.onDisconnect().subscribe(this.onOffline);

        this.platform.resume.subscribe(this.onResume);

        return new Promise(async (resolve, reject) => {
            await this.requestAppUpdate();
            await this.fetchUpdate();
            resolve();
        });
    }

    fetchUpdate = () => {
        return new Promise((resolve, reject) => {
            if (typeof window['cordova'] !== 'undefined') {
                console.log('Update is loaded...');

                /*if (this.cartManager.get('resolve')) {
                    this.apiCallbackResolve();

                    return;
                } else {
                    this.cartManager.set('resolve', resolve);
                }*/

                chcp.fetchUpdate((err, data) => {
                    if (err) {
                        console.log(err);
                        //this.apiCallbackResolve();
                        resolve();
                        return;
                    }

                    this.splashScreen.hide();

                    this.alertProvider.create({
                        title: "更新",
                        message: "資料更新中..",
                        enableBackdropDismiss: false,
                        buttons: [{
                            text: "確認",
                            handler: () => {
                                //this.spinner.show();

                                chcp.installUpdate(error => {
                                    //this.spinner.hide();
                                    this.splashScreen.show();

                                    if (error) {
                                        console.log(error);
                                        //this.apiCallbackResolve();
                                        resolve();
                                        return;
                                    }
                                    
                                    resolve();
                                    //this.cartManager.remove('resolve');
                                });
                            }
                        }]
                    });
                });
            } else {
                resolve();
            }
        });
    }

    requestAppUpdate = () => {
        return new Promise((resolve, reject) => {
            if (typeof window['cordova'] !== 'undefined' && !this.isRequestAppUpdate) {
                /*this.appVersion.getVersionNumber().then((deviceVersion) => {
                    this.api.checkAppVersion(this.device.platform, deviceVersion, (isValid) => {
                        if (isValid) {
                            resolve();
                        } else {
                            this.showAppUpdate();
                        }
                    }, (err) => {
                        resolve();
                    });
                });*/
                resolve();
            } else {
                resolve();
            }
        });
    }

    showAppUpdate() {
        this.splashScreen.hide();
        this.alertProvider.create({
            title: "更新",
            message: "版本已更新，請更新以繼續使用",
            enableBackdropDismiss: false,
            buttons: [{
                text: "確認",
                handler: () => {
                    this.alertProvider.clear();
                    //window.open(Constant.APP_DOWNLOAD_URL, '_system',
                    //    'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
                    cordova.InAppBrowser.open(Constant.APP_DOWNLOAD_URL, '_system', 'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
                    this.showAppUpdate();
                }
            }]
        });
    }

    /*apiCallbackResolve = () => {
        let resolve = this.cartManager.get('resolve');
        resolve();
        this.cartManager.remove('resolve');
    }*/

    onOnline = () => {
        // this.spinner.hide();
    }

    onOffline = () => {
        console.log('offline');
        // this.spinner.show();
    }

    onResume = async () => {
        console.log('Resume');
        await this.requestAppUpdate();
        await this.fetchUpdate();
    }

    loadDeviceInfo = () => {
        return new Promise((resolve, reject) => {
            if (typeof window['cordova'] === 'undefined') {
                localStorage.deviceId = 'testdeviceId';
                localStorage.deviceType = 'testdeviceType';
                localStorage.deviceModel = 'testdeviceModel';
                localStorage.deviceResolution = window.screen.width + "@" + window.screen.height + "@" + window.innerWidth + "@" + window.innerHeight;
                localStorage.deviceVersion = 'testdeviceVersion';
                localStorage.deviceManufacturer = 'testdeviceManufacturer';
                localStorage.deviceToken = 'testdeviceToken';

                resolve();
            } else {
                let deviceType = this.device.platform;
                let deviceModel = this.device.model;
                let deviceResolution = window.screen.width + "@" + window.screen.height + "@" + window.innerWidth + "@" + window.innerHeight;
                let deviceVersion = this.device.version;
                let deviceManufacturer = this.device.manufacturer;

                this.uniqueDeviceID.get()
                    .then((uuid: any) => {
                        if (localStorage.deviceType !== deviceType ||
                            localStorage.deviceModel !== deviceModel ||
                            localStorage.deviceResolution !== deviceResolution ||
                            localStorage.deviceVersion !== deviceVersion ||
                            localStorage.deviceManufacturer !== deviceManufacturer ||
                            localStorage.deviceId !== uuid
                        ) {
                            console.log("DeviceInfo");
                            localStorage.deviceType = this.device.platform;
                            localStorage.deviceModel = this.device.model
                            localStorage.deviceResolution = window.screen.width + "@" + window.screen.height + "@" + window.innerWidth + "@" + window.innerHeight;
                            localStorage.deviceVersion = this.device.version;
                            localStorage.deviceManufacturer = this.device.manufacturer;
                            localStorage.deviceId = uuid;

                            //this.userProvider.setDeviceInfo();
                        }
                        resolve();
                    })
                    .catch((error: any) => {
                        console.log(error);
                        resolve();
                    });
            }
        });
    }

    fcmConfig = () => {
        if (typeof window['cordova'] === 'undefined') return;

        this.fcm.getToken().then(token => {
            console.log("getToken");

            if (token !== localStorage.deviceToken) {
                console.log("resetToken");
                localStorage.deviceToken = token;
                // this.userProvider.setDeviceInfo();
            }

        })

        this.fcm.onTokenRefresh().subscribe(token => {
            localStorage.deviceToken = token;
            // this.userProvider.setDeviceInfo();
        })

        this.fcm.onNotification().subscribe(data => {
            console.log("Received");
            console.log(data);
            
            if(!this.cartManager.get("isOrdering")) {
                this.alertProvider.create({
                    title: data.title,
                    message: data.message,
                    enableBackdropDismiss: false,
                    buttons: [{
                        text: "確認",
                    }]
                });
            }

            if (data.wasTapped) {
                console.log("Received in background");
            } else {
                console.log("Received in foreground");
            };
        })
    }
}
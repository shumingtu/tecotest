import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Constant } from '../app/constant';
import { SpinnerProvider } from '../providers/spinnerProvider';

@Injectable()
export class Api {

    constructor(private http: HttpClient, private spinnerProvider: SpinnerProvider) {
    }

    handleHttpSuccess(data, onSuccess, onError) {
        if (data.result == '0') {
            onError(data.message);
        } else {
            onSuccess(data);
        }
    }

    handleHttpError(httpStatus, onError) {
        if (httpStatus === 500) {
            console.log('系統忙碌中');
            onError();
        } else {
            console.log('系統忙碌中，請稍候。');
            onError();
        }
    }

    login(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Token/SignIn", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    editPassword(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Password/Modify", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getAdvertisement(onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.get(Constant.TECO_API + "Advertisement/Get").subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getWarehouseID(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "WareHouse/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getProducts(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "ProductR/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getPresents(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Giveaway/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getInventory(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Inventory/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    /*getWorkDays(onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.get(Constant.TECO_API + "WorkDay/Get").subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }*/

    getWorkDays(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "WorkDay/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    addOrder(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Order/Add", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getOrderList(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Order/Query", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getMessage(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.TECO_API + "Message/Get", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    checkAppVersion(deviceType, version, onSuccess, onError) {
        this.http.get(Constant.TECO_API + "App/CheckAppVersion?deviceType=" + deviceType + "&version=" + version).subscribe(
            data => {
                //this.handleHttpSuccess(data, onSuccess, onError);
                onSuccess(data);
            },
            error => {
                //console.log(error);
                this.handleHttpError(error.status, onError);
            });
    }

    setDeviceInfo(para, onSuccess, onError) {
        this.http.post(Constant.TECO_API + "App/SetDeviceInfo", para).subscribe(
            data => {
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }
}
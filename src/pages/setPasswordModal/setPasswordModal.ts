import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { Api } from '../../services/api';

import * as _ from "lodash";
@Component({
  selector: 'page-set-password-modal',
  templateUrl: 'setPasswordModal.html',
})
export class SetPasswordModalPage {
  account: string;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  message: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    public api: Api,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'setPasswordModal', true);
  }
  ionViewWillLoad() {
    let isFirstLogin = this.navParams.get('firstLogin');
    console.log(isFirstLogin);

    if (isFirstLogin) {
      this.message = "初次登入請修改密碼";
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialOrderModalPage');
  }

  submit() {
    this.message = "";

    if (!this.account || !this.oldPassword || !this.newPassword || !this.confirmPassword) {
      console.log("null data");
      this.message = "欄位不可空白";
      return false;
    }

    if(this.newPassword == this.oldPassword) {
      console.log("same password");
      this.message = "新舊密碼不可相同";
      return false;
    }

    if(this.newPassword !== this.confirmPassword) {
      console.log("confirm password not same");
      this.message = "確認密碼不一致";
      return false;
    }


    if(this.newPassword.length < 4 || this.newPassword.length > 20) {
      console.log("password length error");
      this.message = "密碼長度要在 4 ~ 20 之間";
      return false;
    }


    let tempData = {
      //"ACCOUNT": "44959704",
      //"PASSWORDOLD": "44959704",
      //"PASSWORDNEW": "44959704"
      "ACCOUNT": this.account,
      "PASSWORDOLD": this.oldPassword,
      "PASSWORDNEW": this.newPassword
    }

    this.api.editPassword(tempData, (result) => {
      console.log(result);
       
      this.viewCtrl.dismiss("success");
    }, (err) => {
      console.log(err);

      this.message = "修改失敗";
    })

    //this.viewCtrl.dismiss("submit");
  }

  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss("close");
  }

}

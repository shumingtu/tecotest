import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertProvider } from '../../providers/alertProvider';
import { AcPage } from '../ac/ac';
import { AddressPage } from '../address/address';


@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {

  constructor(public navCtrl: NavController, public alertProvider: AlertProvider,) {

  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toAcPage() {
    this.navCtrl.push(AcPage, { animate: false });
  }

  toHome() {
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }

  showdisableMessage() {
    this.alertProvider.create({
      title: "商品尚未上架",
      message: "敬請期待",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "確認",
        }
      ]
    });
    return;
  }
}

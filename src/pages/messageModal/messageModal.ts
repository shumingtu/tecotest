import { Component, Renderer } from '@angular/core';
import { ViewController, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-message-modal',
  templateUrl: 'messageModal.html',
})
export class MessageModalPage {
  message = {
    messageId: "",
    title: "",
    message: "",
    createTime: null,
  };

  goHome = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'messageModal', true);
  }
  ionViewWillLoad() {
    this.message = this.navParams.get('message');
    console.log(this.message);
    this.message.message = this.message.message.replace(/\n/g, "<br />");
    console.log(this.message);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodModalPage');
  }

  back() {
    this.viewCtrl.dismiss();
  }

  toHome() {
    this.goHome = true;
    this.viewCtrl.dismiss(this.goHome);
  }
}

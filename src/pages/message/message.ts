import { Component } from '@angular/core';
import { NavController, ModalController, } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Api } from '../../services/api';

import { MessageModalPage } from '../messageModal/messageModal';

import * as _ from "lodash";

@Component({
  selector: 'page-message',
  templateUrl: 'message.html'
})
export class MessagePage {
  accountData: any;

  messageList = [];
  oldRecord = {};
  newRecord = {};

  /*messageList = [
    {
      messageId: "20200510001",
      createTime: "2020/05/10 10:30",
      title: "訂單已下單",
      message: "經銷商 王小明 於2020/05/10 10:30發出之委託訂單<br>4 項商品已下單，0 項商品缺貨中<br>送貨日期：2020/05/13",
    },
    {
      messageId: "20200601002",
      createTime: "2020/06/01 14:00",
      title: "訂單已下單",
      message: "經銷商 王小明 於2020/06/01 14:00發出之委託訂單<br>4 項商品已下單，3 項商品缺貨中<br>送貨日期：2020/06/03",
    }
  ];*/

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public api: Api,) {
    this.accountData = JSON.parse(localStorage.getItem('accountData'));
  }

  ionViewWillEnter() {
    this.refreshData()
  }

  refreshData() {
    let para = {
      "ID": this.accountData.ID,
    };

    this.api.getMessage(para, (data) => {
      console.log(data);
      this.messageList = data.data;

      //處理已讀狀態
      let tempStr = localStorage.getItem('readMessages');
      this.oldRecord = tempStr ? JSON.parse(tempStr) : {};
      this.newRecord = {};
      this.resetRecord(this.messageList);
    }, () => {
      console.log("data error");
    });
  }

  async resetRecord(messages) {
    let refreshRecord = await this.refreshRecord(messages);
    let setRecord = await this.setRecord();
    console.log(this.newRecord);
    localStorage.setItem('readMessages', JSON.stringify(this.newRecord));
    this.getUnReadCount();
  }

  async refreshRecord(messages) {
    await messages.forEach( message => {
      this.newRecord[message.messageId] = 0;
    })
  }

  async setRecord() {
    await _.forOwn(this.oldRecord, (value, key) => {
      if (this.newRecord.hasOwnProperty(key)) {
        if (value == 1) {
          this.newRecord[key] = 1;
        }
      }
    })
  }

  getUnReadCount(){
    console.log(this.newRecord);
    let unReadCount = 0
    for(let key in this.newRecord){
        if(this.newRecord[key] == 0){
            unReadCount++;
        }
    }

    //this.events.publish('tab:refreshBadge', unReadCount);

    //return unReadCount;
    console.log(unReadCount);
  }

  openMessageModal(message) {
    const messageModal = this.modalCtrl.create(MessageModalPage, {
      message: message
    });
    messageModal.onDidDismiss( goHome => {
      console.log("close modal");
      this.getUnReadCount();
      /*if(goHome){
        this.navCtrl.popToRoot({ animate: true, direction: 'back' });
      }*/
    });
    messageModal.present();

    //標記已讀訊息
    this.newRecord[message.messageId] = 1;
    localStorage.setItem('readMessages', JSON.stringify(this.newRecord));
  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

}

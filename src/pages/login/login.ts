import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

//import { DateModalPage } from '../dateModal/dateModal';
import { SetPasswordModalPage } from '../setPasswordModal/setPasswordModal';

import { AlertProvider } from '../../providers/alertProvider';
import { UserProvider } from '../../providers/userProvider';

import { Api } from '../../services/api';

import * as moment from "moment";
import { HomePage } from '../home/home';
//import { SetPasswordPage } from '../setPassword/setPassword';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  account: string;
  password: string;

  tempData = {
    "ACCOUNT": "",
    "PASSWORD": "",
    "DEVICE": ""
  }

  /*tempData = {
    "ACCOUNT": "06316293",
    "PASSWORD": "06316293",
    "DEVICE": "D06316293Test"
  }*/

  /*tempData = {
    "ACCOUNT": "01189353",
    "PASSWORD": "01189353",
    "DEVICE": "D01189353Test"
  }*/

  /*tempData = {
    "ACCOUNT": "22098787",
    "PASSWORD": "22098787",
    "DEVICE": "D22098787Test"
  }*/

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public modalCtrl: ModalController,
    public alertProvider: AlertProvider,
    public userProvider: UserProvider,) {
  }

  ionViewWillEnter() {
    //清空localStorage
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('accountData');
    localStorage.removeItem('creditData');
  }

  login () {
    console.log("account=" + this.account);
    console.log("password=" + this.password);

    if (!this.account || !this.password) {
      console.log("no data");
      this.alertProvider.create({
        title: "登入資料錯誤",
        message: "請輸入統編和密碼",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
            handler: () => {
            }
          }
        ]
      });

      return;
    }

    if (this.password.length < 4 || this.password.length > 20) {
      console.log("no data");
      this.alertProvider.create({
        title: "登入資料錯誤",
        message: "密碼長度要在 4 ~ 20 之間",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
            handler: () => {
            }
          }
        ]
      });

      return;
    }


    this.tempData.ACCOUNT = this.account;
    this.tempData.PASSWORD = this.password;

    this.api.login(this.tempData, (result) => {
      console.log(result);
      //this.message = "登入成功";
      //登入後撈回資料
      localStorage.setItem('userId', this.tempData.ACCOUNT);
      localStorage.setItem('token', result.TokenData.TOKEN);
      localStorage.setItem('accountData', JSON.stringify(result.CustomerData[0]));

      if (result.CreditData.length > 0) {
        localStorage.setItem('creditData', JSON.stringify(result.CreditData[0]));
      } else {
        this.alertProvider.create({
          title: "帳號資料錯誤",
          message: "沒有信限資料，請洽客服處理",
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "確認",
              handler: () => {
              }
            }
          ]
        });
        return;
      }

      //記錄裝置資訊
      this.userProvider.setDeviceInfo();

      //初次登入(密碼還是預設的統編)跳出修改密碼modal
      if (this.tempData.ACCOUNT == this.tempData.PASSWORD) {
        const setPasswordModal = this.modalCtrl.create(SetPasswordModalPage, {
          firstLogin: true,
        }, { showBackdrop: false, enableBackdropDismiss: false });
        setPasswordModal.present();
        setPasswordModal.onDidDismiss((drug) => {
          console.log(drug);
        });
      }
       
      this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
    }, (err) => {
      //console.log("return err");
      console.log(err);
      this.alertProvider.create({
        message: "帳號密碼錯誤",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
            handler: () => {
            }
          }
        ]
      });
      //this.message = err;
    })

    //localStorage.setItem('accountData', JSON.stringify({"ID":"D44959704","NAME":"中全企業有限公司","TYPE":"K000","GROUP":"02","NOTE":"經銷商--非批店","USER":"吳松祈","ADDRESS":"桃園縣中壢市三光路140-1 號　　　　　　　　　　","DEPARTMENT":"08"}));
    //this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }

  /*toSetPasswordPage() {
    this.navCtrl.push(SetPasswordPage, { animate: false });
    console.log("to setPassword");
  }*/
}

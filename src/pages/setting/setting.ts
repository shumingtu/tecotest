import { Component } from '@angular/core';
import { NavController, ModalController, } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SetPasswordModalPage } from '../setPasswordModal/setPasswordModal';
import { Constant } from '../../app/constant';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html'
})
export class SettingPage {
  accountData: any;
  appVersion: any;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,) {
    this.accountData = JSON.parse(localStorage.getItem('accountData'));
    this.appVersion = Constant.APP_VERSION;
  }

  openSetPasswordModal(food) {
    console.log(food);
    const setPasswordModal = this.modalCtrl.create(SetPasswordModalPage, {
      //drugOptions: this.info.drugOptions,
    }, { showBackdrop: false, enableBackdropDismiss: false });
    setPasswordModal.present();
    setPasswordModal.onDidDismiss((drug) => {
      console.log(drug);
    });

  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toHome() {
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }
}

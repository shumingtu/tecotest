import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderListPage } from '../orderList/orderList';
import { AcPage } from '../ac/ac';
import { CategoryPage } from '../category/category';
import { Constant } from '../../app/constant';
import { CartManager } from '../../services/cartManager';
import { AlertProvider } from '../../providers/alertProvider';
import { Api } from '../../services/api';

import * as moment from "moment";

@Component({
  selector: 'page-address',
  templateUrl: 'address.html'
})
export class AddressPage {
  accountData: any;

  deliveryData = {
    deliveryType: 1,
    city: "",
    district: "",
    address: "",
    receiver: "",
    phone: "",
    cellPhone: "",
  }

  noteData = {
    noteOption: "",
    noteText: "",
  }

  // deliveryType: number = 1;
  //selectedCity: string = "";
  //selectedDistrict: string = "";x
  //address: string = "";
  //phone: string = "";
  //cellPhone: string = "";
  //note: string = "";
  //receiver: string = "";

  cities = [];
  districts = {};
  noteOptions = ["C.自取", "D.送貨", "E.急件", "F.拆箱定位"];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertProvider: AlertProvider, public cartManager: CartManager, public api: Api,) {
    this.cities = Constant.CITIES;
    this.districts = Constant.DISTRICTS;

    this.accountData = JSON.parse(localStorage.getItem('accountData'));

    if (localStorage.warehouseID) {
      localStorage.removeItem("warehouseID");
    }
    if (localStorage.deliveryData) {
      localStorage.removeItem("deliveryData");
    }
    if (localStorage.noteData) {
      localStorage.removeItem("noteData");
    }
  }

  changeCity() {
    this.deliveryData.district = "";
  }

  toCategoryPage() {
    let tempAddress = "";

    //檢查欄位
    if (this.deliveryData.deliveryType == 1) {
      tempAddress = this.accountData.ADDRESS.trim();
      console.log(tempAddress);

      //查本地倉代號
      let para = {
        "ADDRESS": tempAddress,
      };

      this.api.getWarehouseID(para, (data) => {
        console.log(data);
        localStorage.setItem('warehouseID', data.data.CODE);
        
      }, () => {
        console.log("data error");
        this.alertProvider.create({
          title: "地址格式過舊",
          message: "請改用指定送貨填寫地址，或通知客服更新地址資料。",
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "確認",
            }
          ]
        });

        if (localStorage.warehouseID) {
          localStorage.removeItem("warehouseID");
        }
        return false;
      });

    } else if (this.deliveryData.deliveryType == 2) {
      if (this.deliveryData.city == "" ||
          this.deliveryData.district == "" ||
          this.deliveryData.address == "" ||
          this.deliveryData.receiver == "" ||
          this.deliveryData.phone == "" ||
          this.deliveryData.cellPhone == "") {
            this.alertProvider.create({
              title: "指定店址欄位不能為空",
              message: "請補齊除交貨指示碼和備註以外所有欄位。",
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: "確認",
                }
              ]
            });
            return false;
      }

      tempAddress = this.deliveryData.city + this.deliveryData.district + this.deliveryData.address;
      console.log(tempAddress);

      //查本地倉代號
      let para = {
        "ADDRESS": tempAddress,
      };
      this.api.getWarehouseID(para, (data) => {
        console.log(data);
        localStorage.setItem('warehouseID', data.data.CODE);
        
      }, () => {
        console.log("data error");
        this.alertProvider.create({
          title: "地址格式錯誤",
          message: "請通知客服處理。",
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "確認",
            }
          ]
        });

        if (localStorage.warehouseID) {
          localStorage.removeItem("warehouseID");
        }
        return false;
      });
    }

    if (this.noteData.noteText.length > 20) {
      this.alertProvider.create({
        title: "備註欄位過長",
        message: "長度不可超過20個字。",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
          }
        ]
      });
      return false;
    }

    console.log(this.deliveryData);
    console.log(this.noteData);
    localStorage.setItem('deliveryData', JSON.stringify(this.deliveryData));
    localStorage.setItem('noteData', JSON.stringify(this.noteData));

    this.navCtrl.push(CategoryPage, { animate: false });
  }

  setDeliveryType(type) {
    console.log(type);
    this.deliveryData.deliveryType = type;
  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toHome() {
    this.cartManager.clear();
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }
}

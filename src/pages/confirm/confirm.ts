import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderListPage } from '../orderList/orderList';
import { Constant } from '../../app/constant';
import { CartManager } from '../../services/cartManager';
import { AlertProvider } from '../../providers/alertProvider';
import { Api } from '../../services/api';

import * as moment from "moment";

@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html'
})
export class ConfirmPage {
  accountData: any;

  orderItems = [];
  transferDay: number = 0;
  //deliveryType: number = 1;
  selectedYear: any;
  selectedMonth: any;
  deliveryDayOptions: any = [];
  selectedDate: any;
  //selectedCity: string = "";
  //selectedDistrict: string = "";
  //address: string = "";
  //phone: string = "";
  //cellPhone: string = "";
  //note: string = "";
  //receiver: string = "";
  //fee: number = 500;
  totalPrice: number = 0;

  deliveryData = {};
  noteData = {};
  warehouse: any;

  //cities = [];
  //districts = {};
  //noteOptions = ["B.貨已送", "C.自取", "D.送貨", "E.急件", "F.拆箱定位",];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertProvider: AlertProvider, public cartManager: CartManager, public api: Api,) {
    this.accountData = JSON.parse(localStorage.getItem('accountData'));
    this.transferDay = this.navParams.get('transferDay') ? this.navParams.get('transferDay') : 0;

    let param = { transferDay: this.transferDay };
    console.log(param);

    this.api.getWorkDays(param, (data) => {
      this.deliveryDayOptions = data.data;

      //日期預設為第一天
      this.selectedDate = this.deliveryDayOptions[0].date;
      this.selectedYear = this.deliveryDayOptions[0].date.toString().substr(0, 4);
      this.selectedMonth = this.deliveryDayOptions[0].date.toString().substr(4, 2);

      for(let i = 0; i < 14; i++) {
        this.deliveryDayOptions[i].day = this.deliveryDayOptions[i].date.toString().substr(6, 2);
      }
    }, () => {
      console.log("data error");
    })
  }

  ionViewWillEnter() {
    //地址資料
    this.deliveryData = JSON.parse(localStorage.getItem('deliveryData'));
    console.log(this.deliveryData);
    this.noteData = JSON.parse(localStorage.getItem('noteData'));
    console.log(this.noteData);
    this.warehouse = localStorage.getItem('warehouseID');
    console.log(this.warehouse);

    this.orderItems = this.navParams.get('order');
    console.log(this.orderItems);
    //this.calcTotalPrice();
  }

  selectDate(option) {
    this.selectedDate = option.date;
    this.selectedYear = option.date.toString().substr(0, 4);
    this.selectedMonth = option.date.toString().substr(4, 2);
    console.log(this.selectedDate);
  }

  calcTotalPrice() {
    this.totalPrice = 0;
    let tempTotalPrice = 0;

    this.orderItems.forEach(item => {
      if (item.isSelected) {
        //商品價格
        tempTotalPrice += item.PRODUCTPRICE[item.selectedPriceType].PRICE * item.amount;

        //關聯性商品價格
        if (item.RELATEDPRODUCTCODE.length > 5 ) {
          tempTotalPrice += item.RELATEDPRODUCTPRICE[item.selectedPriceType].PRICE * item.relatedItemAmount;
        }

      }
    });

    //運費
    //tempTotalPrice += this.fee;

    this.totalPrice = tempTotalPrice;
  }

  submit() {
    //let orderAddress = this.selectedCity + this.selectedDistrict + this.address;
    if (!this.selectedDate) {
      this.alertProvider.create({
        title: "資料錯誤",
        message: "請選擇送貨日期。",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
            handler: () => {
            }
          }
        ]
      });
      return;
    }


    console.log(this.deliveryData);
    console.log(this.noteData);
    console.log(this.selectedDate);
    console.log(this.orderItems);

    let order = {
      ID: "",
      DEPARTMENT: "",
      WAREHOUSE: "",
      TYPE: "",
      CITY: "",
      ADDRESS: "",
      PHONE: "",
      MOBILE: "",
      RECEIVER: "",
      DELIVERYINSTRUCTIONCODE: "",
      NOTE: "",
      SHIPPINGTIME: 0,
      data: []
    };

    // 組出訂單資料
    order.ID = this.accountData['ID'];
    order.DEPARTMENT = "",
    order.WAREHOUSE = this.warehouse ? this.warehouse : "K500";
    console.log(this.deliveryData['deliveryType']);
    if (this.deliveryData['deliveryType'] == 1) { //客戶主檔地址
      console.log("type 1");
      order.TYPE = "OR";
      order.CITY = "";
      order.ADDRESS = "";
      order.PHONE = "";
      order.MOBILE = "";
      order.RECEIVER = "";
      order.DELIVERYINSTRUCTIONCODE = (this.noteData['noteOption'] ? this.noteData['noteOption'].substring(0, 1) : "");
      order.NOTE = this.noteData['noteText'] + " " + this.selectedDate + " 送貨";
      order.SHIPPINGTIME = this.transferDay;
    } else if (this.deliveryData['deliveryType'] == 2) { //指定地址
      console.log("type 2");
      order.TYPE = "OR";
      order.CITY = this.deliveryData['city'];
      order.ADDRESS = this.deliveryData['address'];
      order.PHONE = this.deliveryData['phone'];
      order.MOBILE = this.deliveryData['cellPhone'];
      order.RECEIVER = this.deliveryData['receiver'];
      order.DELIVERYINSTRUCTIONCODE = (this.noteData['noteOption'] ? this.noteData['noteOption'].substring(0, 1) : "");
      order.NOTE = this.noteData['noteText'] + " " + this.selectedDate + " 送貨";
      order.SHIPPINGTIME = this.transferDay;
    } else if (this.deliveryData['deliveryType'] == 3) { //寄代管倉
      console.log("type 3");
    } else {
      console.log("type error");
    }

    //加入訂單商品資料
    this.orderItems.forEach(item => {
      let tempGift = [];

      //01價才能買贈品
      if (item.selectedPriceType == 0) {
        item.presentItems.forEach(gift => {
          tempGift.push(
            {
              "PRODUCTCODE": gift.model,
              "PRODUCTQUANTITY": gift.amount
            }
          );
        })
      }

      order.data.push(
        {
          "PRICETYPE": (item.selectedPriceType == 0) ? "01" : "18",
          "PRODUCTCODE": item.PRODUCTCODE,
          "PRODUCTQUANTITY": item.amount,
          "RELATEDPRODUCTCODE": item.RELATEDPRODUCTCODE,
          "RELATEDPRODUCTQUANTITY": item.relatedItemAmount,
          "Giveaway": Array.from(tempGift)
        }
      )
    }); 

    console.log(order);
    /*let tempOrder = 
    {
      "ID": "D01189353",
      "DEPARTMENT": "24",
      "WAREHOUSE": "K500",
      "TYPE": "OR",
      "PURCHASEORDERCODE": "",
      "CITY": "",
      "ADDRESS": "",
      "PHONE": "",
      "MOBILE": "",
      "RECEIVER": "",
      "DELIVERYINSTRUCTIONCODE": "D",
      "NOTE": "api test",
      "data": [
        {
          "PRICETYPE": "01",
          "PRODUCTCODE": "MA36IC-HS",
          "PRODUCTQUANTITY": "1",
          "RELATEDPRODUCTCODE": "MA28IC-HS",
          "RELATEDPRODUCTQUANTITY": "1",
          "Giveaway": [
            {
              "PRODUCTCODE": "NN1601BD",
              "PRODUCTQUANTITY": "1"
            }
          ]
        },
        {
          "PRICETYPE": "01",
          "PRODUCTCODE": "MA40IC-HS",
          "PRODUCTQUANTITY": "1",
          "RELATEDPRODUCTCODE": "",
          "RELATEDPRODUCTQUANTITY": "",
          "Giveaway": [
            {
              "PRODUCTCODE": "NN1601BD",
              "PRODUCTQUANTITY": "1"
            }
          ]
        },
        {
          "PRICETYPE": "01",
          "PRODUCTCODE": "MA28IC-HS",
          "PRODUCTQUANTITY": "1",
          "RELATEDPRODUCTCODE": "",
          "RELATEDPRODUCTQUANTITY": "",
          "Giveaway": null
        } 
      ]
    };*/
    this.alertProvider.create({
      title: "訂單即將送出",
      message: "是否下單?",
      buttons: [
        {
          text: "取消",
        },
        {
          text: "確認下單",
          handler: () => {
            this.api.addOrder(order, (data) => {
              console.log(data);
        
              this.alertProvider.create({
                title: "委託訂單已送出",
                message: "五分鐘內會處理下單。",
                enableBackdropDismiss: false,
                buttons: [
                  {
                    text: "確認",
                    handler: () => {
                      //清空購物車資料
                      this.cartManager.clear();
                      localStorage.removeItem("deliveryData");
                      localStorage.removeItem("noteData");
        
                      this.navCtrl.push(OrderListPage).then(() => {
                        const index = this.navCtrl.getActive().index;
                        console.log(index);
                        this.navCtrl.remove(1, index-1);
                      });
                    }
                  }
                ]
              });
            }, () => {
              console.log("data error");
        
              this.alertProvider.create({
                title: "訂單錯誤",
                message: "請重新送出或洽詢客服。",
                enableBackdropDismiss: false,
                buttons: [
                  {
                    text: "確認",
                  }
                ]
              });
            })
          }
        }
      ]
    });


    /*this.alertProvider.create({
      title: "委託訂單已送出",
      message: "訂單送出五分鐘內可修改或刪除。",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "確認",
          handler: () => {
            //清空購物車資料
            this.cartManager.clear();
            localStorage.removeItem("deliveryData");
            localStorage.removeItem("noteData");

            this.navCtrl.push(OrderListPage).then(() => {
              const index = this.navCtrl.getActive().index;
              console.log(index);
              this.navCtrl.remove(1, index-1);
            });
          }
        }
      ]
    });*/

  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toHome() {
    this.cartManager.clear();
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }
}

import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { Constant } from '../../app/constant';
import { CartManager } from '../../services/cartManager';
import { AlertProvider } from '../../providers/alertProvider';
import { OrderDetailModalPage } from '../orderDetailModal/orderDetailModal';
import { Api } from '../../services/api';

import * as moment from "moment";

@Component({
  selector: 'page-orderList',
  templateUrl: 'orderList.html'
})
export class OrderListPage {
  orderStatusType: string[];
  accountData: any;

  orderList = [];
  /*orderList = [
    {
      orderId: "20200510001",
      createTime: "2020/05/10 10:00",
      deliveryDate: "2020/05/13",
      status: 0,
      deliveryType: 2,
      city: "台北市",
      district: "中山區",
      address: "南京東路五段",
      receiver: "王小明",
      phone: "0211112222",
      cellPhone: "0911222333",
      noteOption: "D. 急件",
      noteText: "321",
      item:[
        {
          name: "分離式", model: "", price: 10000, amount: 2, status: "已到貨", deliveryCode: "80000001",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 2, relatedStatus: "已到貨", relatedDeliveryCode: "80000001",
          present: [
            { name: "贈品一", model: "", price: 100, amount: 1, status: "已到貨", deliveryCode: "80000002",},
            { name: "贈品二", model: "", price: 200, amount: 1, status: "已到貨", deliveryCode: "80000002",},
          ]
        }
      ],
    },
    {
      orderId: "20200510001",
      createTime: "2020/05/10 10:00",
      deliveryDate: "2020/05/13",
      status: 1,
      deliveryType: 2,
      city: "台北市",
      district: "中山區",
      address: "南京東路五段",
      receiver: "王小明",
      phone: "0211112222",
      cellPhone: "0911222333",
      noteOption: "D. 急件",
      noteText: "321",
      item:[
        {
          name: "分離式", model: "", price: 10000, amount: 2, status: "已到貨", deliveryCode: "80000001",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 2, relatedStatus: "已到貨", relatedDeliveryCode: "80000001",
          present: [
            { name: "贈品一", model: "", price: 100, amount: 1, status: "已到貨", deliveryCode: "80000002",},
            { name: "贈品二", model: "", price: 200, amount: 1, status: "已到貨", deliveryCode: "80000002",},
          ]
        }
      ],
    },
    {
      orderId: "20200601002",
      createTime: "2020/06/01 14:00",
      deliveryDate: "2020/06/03",
      status: 2,
      deliveryType: 1,
      city: "",
      district: "",
      address: "",
      receiver: "",
      phone: "",
      cellPhone: "",
      noteOption: "D. 急件",
      noteText: "321",
      item:[
        {
          name: "分離式", model: "", price: 10000, amount: 3, status: "已下單", deliveryCode: "80000003",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 3, relatedStatus: "已下單", relatedDeliveryCode: "80000003",
          present: [
            { name: "贈品一", model: "", price: 100, amount: 2, status: "已下單", deliveryCode: "80000004",},
            { name: "贈品二", model: "", price: 200, amount: 1, status: "已下單", deliveryCode: "80000004",},
          ]
        },
        {
          name: "分離式", model: "", price: 10000, amount: 4, status: "已下單", deliveryCode: "80000003",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 3, relatedStatus: "已下單", relatedDeliveryCode: "80000003",
          present: [
            { name: "贈品三", model: "", price: 300, amount: 2, status: "已下單", deliveryCode: "80000004",},
            { name: "贈品四", model: "", price: 400, amount: 2, status: "已下單", deliveryCode: "80000004",},
          ]
        },
        {
          name: "窗機", model: "", price: 10000, amount: 2, status: "缺貨中", deliveryCode: "80000003",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 2, relatedStatus: "缺貨中", relatedDeliveryCode: "80000003",
          present: [
            { name: "贈品五", model: "", price: 500, amount: 2, status: "缺貨中", deliveryCode: "80000003",},
          ]
        },
      ],
    },
    {
      orderId: "20200510001",
      createTime: "2020/05/10 10:00",
      deliveryDate: "2020/05/13",
      status: 3,
      deliveryType: 1,
      city: "",
      district: "",
      address: "",
      receiver: "",
      phone: "",
      cellPhone: "",
      noteOption: "D. 急件",
      noteText: "321",
      item:[
        {
          name: "分離式", model: "", price: 10000, amount: 2, status: "已到貨", deliveryCode: "80000001",
          relatedName: "室內機", relatedModel: "", relatedPrice: 10000, relatedAmount: 2, relatedStatus: "已到貨", relatedDeliveryCode: "80000001",
          present: [
            { name: "贈品一", model: "", price: 100, amount: 1, status: "已到貨", deliveryCode: "80000002",},
            { name: "贈品二", model: "", price: 200, amount: 1, status: "已到貨", deliveryCode: "80000002",},
          ]
        }
      ],
    },
  ];*/

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public alertProvider: AlertProvider, public cartManager: CartManager, public api: Api,) {
    this.orderStatusType = Constant.orderStatus;
    this.accountData = JSON.parse(localStorage.getItem('accountData'));

    let para = {
      //"ID": "D08001827",
      //"ID": "D01189353",
      "ID": this.accountData.ID
    };

    this.api.getOrderList(para, (data) => {
      console.log(data);
      this.orderList = data.data;
      
    }, () => {
      console.log("data error");
    });

  }


  editOrder(order) {
    //todo 帶訂單資料到購物車頁面
    let orderData = [
      {
        model: "MA22IC-HS",
        rt: "545x730x285",
        priceType: [{type: "01", price: 10000}],
        selectedPriceType: 0,
        amount: 2,
        relatedItem: {
          model: "MS22IE-HS",
          rt: "545x730x285",
          priceType: [{type: "01", price: 10000}],
          amount: 2,
        },
        presentItems: [{ name: "贈品一", model: "001", price: 100, amount: 2 },],
      },
      {
        model: "MA22IH-ZR",
        rt: "545x730x285",
        priceType: [{type: "01", price: 10000}, {type: "18", price: 9500}],
        selectedPriceType: 1,
        amount: 3,
        relatedItem: {
          model: "MS22IH-ZR",
          rt: "545x730x285",
          priceType: [{type: "01", price: 10000}, {type: "18", price: 9500}],
          amount: 3,
        },
        presentItems: [],
      }
    ];
    let deliveryData = {
      deliveryType: 2,
      city: "台北市",
      district: "中山區",
      address: "南京東路五段",
      receiver: "王小明",
      phone: "0211112222",
      cellPhone: "0911222333",
    };
    let noteData = {
      noteOption: "D. 急件",
      noteText: "321",
    };

    let tempOrder = {
      orderData: orderData,
      deliveryData: deliveryData,
      noteData: noteData,
    };

    localStorage.setItem('isEdit', "1");
    //localStorage.setItem('deliveryData', JSON.stringify(deliveryData));
    //localStorage.setItem('noteData', JSON.stringify(noteData));
    //this.cartManager.setOrder(tempOrder);
    this.navCtrl.push(CartPage, {tempOrder: tempOrder, animate: false });

  }

  deleteOrder(order) {
    //todo 刪除訂單
    //if 超過五分鐘 => 不送出
    /*let nowTime = moment();
    if (moment(order.createTime).add(5, 'm') < nowTime) {
      console.log("已超出修改時間");
      return;
    }*/

    this.alertProvider.create({
      title: "刪除委託訂單",
      message: "訂單已刪除",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "確認",
          handler: () => {
            this.alertProvider.clear();
          }
        }
      ]
    });
  }

  openOrderDetailModal(order) {
    console.log(order);
    const orderDetailModal = this.modalCtrl.create(OrderDetailModalPage, {
      order: order,
    }, { showBackdrop: false, enableBackdropDismiss: false });
    orderDetailModal.present();
    orderDetailModal.onDidDismiss((order) => {
      console.log(order);
    });

  }

  openDeliverySearch() {
    window.open('https://www.e-can.com.tw/search_Goods.aspx', "_blank");
  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toHome() {
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }
}

import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import * as _ from "lodash";
@Component({
  selector: 'page-product-detail-modal',
  templateUrl: 'productDetailModal.html',
})
export class ProductDetailModalPage {
  product;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'productDetailModal', true);
  }
  ionViewWillLoad() {
    this.product = this.navParams.get('product');
    console.log(this.product);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialOrderModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.product);
  }

}

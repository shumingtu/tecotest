import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CartManager } from '../../services/cartManager';
import { ConfirmPage } from '../confirm/confirm';
import { AlertProvider } from '../../providers/alertProvider';
import { Api } from '../../services/api';

import * as _ from "lodash";

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html'
})
export class CartPage {
  accountData: any;
  deliveryData: any;

  itemList: any;
  inventoryList: any;
  totalPrice: number = 0;
  isSelectAll: boolean = false;
  presentList = [];

  /*relatedItems = [
    { name: "不選購", model: "", price: 0, amount: 0 },
    { name: "室內機", model: "MS22IE-HS", price: 10000, amount: 0 },
    //{ name: "空氣清淨機", model: "12345", price: 5000, amount: 0 }
  ];*/

  testItemList = [
    {
      model: "MA22IC-HS",
      rt: "545x730x285",
      priceType: [{type: "01", price: 10000}],
      amount: 2,
      relatedItem: {
        model: "MS22IE-HS",
        rt: "545x730x285",
        priceType: [{type: "01", price: 10000}],
        amount: 2,
      }
    },
    {
      model: "MA22IH-ZR",
      rt: "545x730x285",
      priceType: [{type: "01", price: 10000}, {type: "18", price: 9500}],
      amount: 3,
      relatedItem: {
        model: "MS22IH-ZR",
        rt: "545x730x285",
        priceType: [{type: "01", price: 10000}, {type: "18", price: 9500}],
        amount: 3,
      }
    }
  ];

  /*presentList = [
    { name: "不選購", model: "", price: 0, amount: 0 },
    { name: "贈品一", model: "001", price: 100, amount: 0 },
    { name: "贈品二", model: "002", price: 200, amount: 0 },
    { name: "贈品三", model: "003", price: 300, amount: 0 },
    { name: "贈品四", model: "004", price: 400, amount: 0 },
    { name: "贈品五", model: "005", price: 500, amount: 0 },
  ];*/

  constructor(public navCtrl: NavController, public navParams: NavParams, public cartManager: CartManager, public alertProvider: AlertProvider, public api: Api,) {
    this.accountData = JSON.parse(localStorage.getItem('accountData'));
    this.deliveryData = JSON.parse(localStorage.getItem('deliveryData'));
  }

  ngOnInit() {
    if (localStorage.isEdit) {
      console.log("is edit");
      //this.itemList = this.testItemList;
      let tempOrder = this.navParams.get('tempOrder');
      this.itemList = tempOrder.orderData;
      localStorage.setItem('deliveryData', JSON.stringify(tempOrder.deliveryData));
      localStorage.setItem('noteData', JSON.stringify(tempOrder.noteData));
      console.log(this.itemList);
      this.refreshTotalPrice();
    } else {
      this.itemList = this.cartManager.list();
      console.log(this.itemList);
      this.itemList.forEach(item => {
        item.selectedPriceType = "0";
        item.amount = 0;
        item.relatedItemAmount = 0;
        item.presentItems = [];
        item.subTotal = 0;
      });
    }

    //預設全選
    this.isSelectAll = false;
    this.selectAll();
    console.log(this.itemList);

    /*this.itemList = this.cartManager.list();
    console.log(this.itemList);
    this.itemList.forEach(item => {
      //item.price = 10000;
      item.selectedPriceType = 0;
      item.amount = 0;
      item.relatedItem.amount = 0;
      //item.present = { name: "", model: "", price: 0, amount: 0 };
      item.presentItems = [];
      item.subTotal = 0;
    });

    this.isSelectAll = true;
    this.selectAll();
    console.log(this.itemList);*/

    //let tempPresentList = [];
    let para = {
      //"ID": "D01189353",
      "ID": this.accountData.ID
    };

    //撈贈品資料
    this.api.getPresents(para, (data) => {
      console.log(data);
      
      //this.presentList = data.data[0].Giveaway;
      this.presentList = data.data;
      //this.presentList.push({ PRODUCTNAME: "不選購", PRODUCTCODE: "", PRICE: 0 });
      console.log(this.presentList);
    }, () => {
      console.log("data error");
    });

  }

  selectItem(item) {
    if (item.isSelected)
      item.isSelected = false;
    else
      item.isSelected = true;
    
    this.checkSelectAll();
  }

  setRadio(item, index) {
    item.selectedPriceType = index;
    this.refreshTotalPrice();
  }

  addPresent(item) {
    if (this.presentList.length > 0) {
      //item.presentItems.push({ name: "", model: "", amount: 1});
      item.presentItems.push({ name: this.presentList[0].PRODUCTNAME, model: this.presentList[0].PRODUCTCODE, amount: 1});
    } else {
      this.alertProvider.create({
        title: "贈品資料錯誤",
        message: "無贈品資料",
        buttons: [{
          text: "確定",
        }]
      });
      return;
    }
  }

  removePresent(item, present) {
    _.remove(item.presentItems, function (presentItem) {
      return presentItem == present;
    });

    this.refreshTotalPrice();
  }

  setPresent(presentItem, model) {
    console.log(model);
    let present = _.find(this.presentList, {'PRODUCTCODE': model});
    console.log(present);
    console.log(presentItem);

    presentItem.name = present.PRODUCTNAME;
    presentItem.model = present.PRODUCTCODE;

    console.log(this.itemList);
    console.log("set present");
    this.refreshTotalPrice();
  }

  /*setItemAmount(item, isAdd) {
    if (isAdd) {
      item.amount++;
    } else {
      if (item.amount > 0) {
        item.amount--;
      }
    }

    item.relatedItemAmount = item.amount;
    this.refreshTotalPrice();
  }*/

  changeAmount(item, amount) {
    console.log(item);
    console.log(amount);
    item.relatedItemAmount = item.amount;
    this.refreshTotalPrice();
  }

  /*setRelatedAmount(item, isAdd) {
    if (isAdd) {
      item.relatedItemAmount++;
    } else {
      if (item.relatedItemAmount > 0) {
        item.relatedItemAmount--;
      }
    }

    this.refreshTotalPrice();
  }*/

  /*setSubAmount(sub, isAdd) {
    if (isAdd) {
      sub.amount++;
    } else {
      if (sub.amount > 0) {
        sub.amount--;
      }
    }
    this.refreshTotalPrice();
  }*/

  refreshTotalPrice() {
    this.totalPrice = 0;
    this.inventoryList = {};
    let tempInventory = [];

    this.itemList.forEach(item => {
      item.subTotal = 0;

      if (item.isSelected) {
        //商品價格
        item.subTotal += item.PRODUCTPRICE[item.selectedPriceType].PRICE * item.amount;

        //組查詢庫存array
        if (_.findIndex(tempInventory, { TYPE: "R000", PRODUCTCODE: item.PRODUCTCODE }) < 0) {
          tempInventory.push({ TYPE: "R000", isGift: false, PRODUCTCODE: item.PRODUCTCODE, QUANTITY: item.amount });
        }

        //關聯性商品價格
        if (item.RELATEDPRODUCTCODE.length > 5) {
          item.subTotal += item.RELATEDPRODUCTPRICE[item.selectedPriceType].PRICE * item.relatedItemAmount;

          //組查詢庫存array
          if (item.relatedItemAmount > 0 && _.findIndex(tempInventory, { TYPE: "R000", PRODUCTCODE: item.RELATEDPRODUCTCODE }) < 0) {
            tempInventory.push({ TYPE: "R000", isGift: false, PRODUCTCODE: item.RELATEDPRODUCTCODE, QUANTITY: item.relatedItemAmount });
          }
        }

        //贈品
        //01價才能買贈品
        if (item.selectedPriceType == 0) {
          item.presentItems.forEach(present => {
            //組查詢庫存array
            let index = _.findIndex(tempInventory, { TYPE: "K000", PRODUCTCODE: present.model });
            console.log(index);
            if (index < 0) {
              tempInventory.push({ TYPE: "K000", isGift: true, PRODUCTNAME: present.name, PRODUCTCODE: present.model, QUANTITY: present.amount });
            } else {
              console.log(tempInventory[index].QUANTITY);
              tempInventory[index].QUANTITY += present.amount;
            }
          });
        }

        this.totalPrice += item.subTotal;

        //自訂地址
        let tempAddress = "";
        if (this.deliveryData.deliveryType  == 2) {
          tempAddress = this.deliveryData.city + this.deliveryData.district + this.deliveryData.address;
        }

        this.inventoryList = {
          //ID: "D01189353",
          //ADDRESS: "台北市內湖區堤頂大道1段215號",
          ID: this.accountData.ID,
          ADDRESS: (this.deliveryData.deliveryType  == 1) ? this.accountData.ADDRESS : tempAddress,
          data: tempInventory
        };
        /*this.inventoryList = {
          ID: "D06316293",
          ADDRESS: "嘉義縣水上鄉回歸村建國四村220號1F",
          data: tempInventory
        };*/
      }
    });
  }

  checkSelectAll() {
    let flag = true;
    
    this.itemList.forEach(item => {
      if (!item.isSelected)
        flag = false;
    });
    
    if (flag)
      this.isSelectAll = true;
    else
      this.isSelectAll = false;

    this.refreshTotalPrice();
  }

  selectAll() {
    console.log("select all");
    this.isSelectAll = !this.isSelectAll;

    if (this.isSelectAll) {
      this.itemList.forEach(item => {
        item.isSelected = true;
      });
    } else {
      this.itemList.forEach(item => {
        item.isSelected = false;
      });
    }

    this.refreshTotalPrice();
  }

  toConfirmPage() {
    console.log("submit");
    console.log(this.itemList);

    let isOrderValid = true;

    //檢查訂單
    this.itemList.forEach(item => {
      //有勾選的才檢查
      if (item.isSelected) {
        if (item.amount < 1) {
          this.alertProvider.create({
            title: "訂單資料錯誤",
            message: "主商品數量不能為零",
            buttons: [{
              text: "確定",
            }]
          });
          isOrderValid = false;
          return;
        }

        //價格不為空
        if (!item.PRODUCTPRICE[item.selectedPriceType].PRICE || !item.RELATEDPRODUCTPRICE[item.selectedPriceType].PRICE) {
          this.alertProvider.create({
            title: "訂單資料錯誤",
            message: "商品金額錯誤，請洽客服",
            buttons: [{
              text: "確定",
            }]
          });
          isOrderValid = false;
          return;
        }

        //贈品數量不能超過一組(主商品和關聯商品各一個)的數量
        if (item.selectedPriceType == 0) {
          let totalAmount = 0;
          item.presentItems.forEach(present => {
            if (present.model) {
              totalAmount += present.amount;
            }
          })
          console.log("totalAmount=" + totalAmount);

          let productAmount = 0;
          productAmount = (item.amount < item.relatedItemAmount) ? item.amount : item.relatedItemAmount;

          console.log("item=" + productAmount);
          
          if (totalAmount > productAmount) {
            this.alertProvider.create({
              title: "訂單資料錯誤",
              message: "贈品數量不能超過商品組數",
              buttons: [{
                text: "確定",
              }]
            });
            isOrderValid = false;
            return;
          }
        }
      }
    });

    console.log(this.inventoryList);

    //查詢庫存
    if (isOrderValid) {
      console.log(this.inventoryList);
      this.api.getInventory(this.inventoryList, (data) => {
        console.log(data);

        //檢查庫存量
        if (data.data.length != this.inventoryList.data.length) {
          console.log("查詢商品數量不合");
          return;
        }

        let isTransfer = true;
        let transferDay = 0;
        let message = "";
        let length = this.inventoryList.data.length;
        for (let i = 0; i < length ; i++) {
          let localCount = parseInt(data.data[i].INVENTORY);
          console.log("庫存");
          console.log(localCount);
          if (data.data[i].INVENTORY == "NoneProduct") {
            if (this.inventoryList.data[i].isGift) {
              message += this.inventoryList.data[i].PRODUCTNAME + " 無資料<br>";
            } else {
              message += this.inventoryList.data[i].PRODUCTCODE + " 無資料<br>";
            }
          }
          else if (localCount < this.inventoryList.data[i].QUANTITY) {
            //本地倉缺貨
            if (data.data[i].SHIPPINGTIME == "X") {
              isTransfer = false;

              //全省庫存不夠
              if (this.inventoryList.data[i].isGift) {
                message += this.inventoryList.data[i].PRODUCTNAME + " 剩餘 " + localCount + " 台，全台缺貨中<br>";
              } else {
                message += this.inventoryList.data[i].PRODUCTCODE + " 剩餘 " + localCount + " 台，全台缺貨中<br>";
              }
            } else {
              //可以移運

              if (this.inventoryList.data[i].isGift) {
                //message += this.inventoryList.data[i].PRODUCTNAME + " 剩餘 " + data.data[i].INVENTORY + " 台<br>";
                message += this.inventoryList.data[i].PRODUCTNAME + " 剩餘 " + localCount + " 台，移運需 " + data.data[i].SHIPPINGTIME + " 天<br>";
                if (parseInt(data.data[i].SHIPPINGTIME) > transferDay) {
                  transferDay = parseInt(data.data[i].SHIPPINGTIME);
                }
              } else {
                //message += this.inventoryList.data[i].PRODUCTCODE + " 剩餘 " + data.data[i].INVENTORY + " 台<br>";
                message += this.inventoryList.data[i].PRODUCTCODE + " 剩餘 " + localCount + " 台，移運需 " + data.data[i].SHIPPINGTIME + " 天<br>";
                if (parseInt(data.data[i].SHIPPINGTIME) > transferDay) {
                  transferDay = parseInt(data.data[i].SHIPPINGTIME);
                }
              }
            }
          }
        };

        if (message) {
          if (isTransfer) {
            console.log("移倉");
            this.alertProvider.create({
              title: "商品缺貨須移運，是否下單?",
              message: message,
              buttons: [
                {
                  text: "取消",
                },
                {
                  text: "確認下單",
                  handler: () => {
                    this.navCtrl.push(ConfirmPage, { order: this.itemList, transferDay: transferDay, animate: false });
                  }
                }
              ]
            });
          } else {
            console.log("缺貨");
            this.alertProvider.create({
              title: "商品缺貨",
              message: message,
              buttons: [
                {
                  text: "確認",
                },
              ]
            });
          }

          return;
        } else {
          console.log("有庫存");
          isOrderValid = true;

          this.navCtrl.push(ConfirmPage, { order: this.itemList, animate: false });
        }

      }, (error) => {
        console.log(error);
        this.alertProvider.create({
          title: "系統資料錯誤",
          message: "請洽客服處理",
          buttons: [
            {
              text: "確認",
            },
          ]
        });
      })
    }


    //todo 檢查信限額度

    //if (isOrderValid) {
    //  this.navCtrl.push(ConfirmPage, { order: this.itemList, animate: false });
    //}
  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toHome() {
    this.cartManager.clear();
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }
}

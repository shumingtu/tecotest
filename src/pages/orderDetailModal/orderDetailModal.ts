import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import { Constant } from '../../app/constant';
import * as _ from "lodash";
@Component({
  selector: 'page-order-detail-modal',
  templateUrl: 'orderDetailModal.html',
})
export class OrderDetailModalPage {
  order;
  orderStatusType: string[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'orderDetailModal', true);
      this.orderStatusType = Constant.orderStatus;
  }
  ionViewWillLoad() {
    this.order = this.navParams.get('order');
    console.log(this.order);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialOrderModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.order);
  }

}

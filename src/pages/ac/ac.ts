import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Content } from 'ionic-angular';
import { CartManager } from '../../services/cartManager';
import { CartPage } from '../cart/cart';
import { ProductDetailModalPage } from '../productDetailModal/productDetailModal';
import { Api } from '../../services/api';

@Component({
  selector: 'page-ac',
  templateUrl: 'ac.html'
})
export class AcPage {
  @ViewChild(Content) content: Content;

  accountData: any;
  productList = [[], [], [], [], [], []];
  type: any;
  searchText: string;
  count: number;
  isOn = 0;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public cartManager: CartManager, public api: Api,) {
    this.type = '1';
    this.count = 0;
    this.accountData = JSON.parse(localStorage.getItem('accountData'));

    //撈商品清單
    let para = {
      //"ID": "D01189353",
      "ID": this.accountData.ID,
      "Type": "ALL"
    };
    this.api.getProducts(para, (data) => {
      console.log(data);
      
      data.data.forEach((product) => {
        switch (product.TYPE) {
          case "1":
            this.productList[0].push(product);
            break;
          case "2":
            this.productList[1].push(product);
            break;
          case "3":
            this.productList[2].push(product);
            break;
          case "4":
            this.productList[3].push(product);
            break;
          case "5":
            this.productList[4].push(product);
            break;
          case "6":
            this.productList[5].push(product);
            break;
          default:
            console.log("type error");
            break;
        }
      })
      console.log(this.productList);
    }, () => {
      console.log("data error");
    });
    
  }

  /*typeChange() {
    this.content.scrollToTop();
  }*/

  typeChange(type) {
    console.log(type);
    this.type = type;
    this.content.scrollToTop();
  }

  toggleSelect() {
    if (this.isOn)
      this.isOn = 0;
    else
      this.isOn = 1;
  }

  openProductDetailModal(product) {
    console.log(product);
    const productDetailModal = this.modalCtrl.create(ProductDetailModalPage, {
      product: product,
    }, { showBackdrop: false, enableBackdropDismiss: false });
    productDetailModal.present();
    productDetailModal.onDidDismiss((product) => {
      console.log(product);
    });

  }

  addToCart(item) {
    console.log(item);
    if(item.isSelected) {
      item.isSelected = false;
      this.cartManager.remove(item);
      this.count--;
    } else {
      item.isSelected = true;
      this.cartManager.add(item);
      this.count++;
    }

    console.log(this.cartManager.list());
  }

  toCartPage() {
    if (localStorage.isEdit) {
      localStorage.removeItem("isEdit");
    }
    
    this.navCtrl.push(CartPage, { animate: false });
  }

  toHome() {
    this.cartManager.clear();
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }

  back() {
    this.cartManager.clear();
    this.navCtrl.pop({ animate: true, direction: 'back' });
  }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertProvider } from '../../providers/alertProvider';
import { SpecAcPage } from '../specAc/specAc';
import { AddressPage } from '../address/address';


@Component({
  selector: 'page-specCategory',
  templateUrl: 'specCategory.html'
})
export class SpecCategoryPage {

  constructor(public navCtrl: NavController, public alertProvider: AlertProvider,) {

  }

  back() {
    this.navCtrl.pop({ animate: true });
  }

  toSpecAcPage() {
    this.navCtrl.push(SpecAcPage, { animate: false });
  }

  toHome() {
    this.navCtrl.popToRoot({ animate: true, direction: 'back' });
  }

  showdisableMessage() {
    this.alertProvider.create({
      title: "商品尚未上架",
      message: "敬請期待",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "確認",
        }
      ]
    });
    return;
  }
}

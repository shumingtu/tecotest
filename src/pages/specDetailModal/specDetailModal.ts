import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import * as _ from "lodash";

declare let cordova: any;

@Component({
  selector: 'page-spec-detail-modal',
  templateUrl: 'specDetailModal.html',
})
export class SpecDetailModalPage {
  product;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'productDetailModal', true);
  }
  ionViewWillLoad() {
    this.product = this.navParams.get('product');
    console.log(this.product);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialOrderModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.product);
  }

  goDataLink(data) {
    console.log("官網頁面");
    console.log(data.TECOURL);
    if(data.TECOURL) {
      cordova.InAppBrowser.open(data.TECOURL, '_system', 'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
    }
  }
}

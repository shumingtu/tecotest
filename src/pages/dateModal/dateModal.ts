import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { CalendarComponentOptions } from 'ion2-calendar'

import * as moment from "moment";

@Component({
  selector: 'page-date-modal',
  templateUrl: 'dateModal.html',
})
export class DateModalPage {
  date: any;
  dateStr: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private modalCtrl: ModalController,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'dateModal', true);
  }
  ionViewWillLoad() {
    this.date = this.navParams.get('date');
    console.log(this.date);
    if (this.date) {
      this.dateStr = moment(this.date).format('YYYY-MM-DD');
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad dateModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.dateStr);
  }

  optionsRange: CalendarComponentOptions = {
    // from: new Date(2011, 1, 7),
    from: moment("2020-01-02", "YYYY-MM-DD").toDate(),
    monthFormat: 'YYYY 年 MM 月 ',
    weekdays: ['日', '一', '二', '三', '四', '五', '六'],
    monthPickerFormat: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
    to: moment("2020-04-15", "YYYY-MM-DD").toDate(),
  };

  onChange($event) {
    console.log($event);
    this.dateStr = moment(this.date).format('YYYY-MM-DD');
  }
}

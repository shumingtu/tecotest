import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddressPage } from '../address/address';
import { OrderListPage } from '../orderList/orderList';
import { MessagePage } from '../message/message';
import { SettingPage } from '../setting/setting';
import { SpecCategoryPage } from '../specCategory/specCategory';
import { AlertProvider } from '../../providers/alertProvider';
import { Api } from '../../services/api';

declare let cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  creditData: any;

  advertisement = [];
  /*advertisement = [
    {
      type: "東元活動",
      title: "東元家電年終好禮大贈送",
      imgUrl: "assets/imgs/img_noimg.svg",
      link: "",
      createTime: "2019-12-01"
    },
    {
      type: "東元活動",
      title: "東元家用空調 暖心好禮5選1",
      imgUrl: "assets/imgs/dm.jpg",
      link: "",
      createTime: "2019-10-01"
    },
    {
      type: "產品新知",
      title: "東元家電 家用空調 [三代同堂篇]",
      imgUrl: "assets/imgs/dm.jpg",
      link: "",
      createTime: "2019-06-11"
    },
    {
      type: "東元公告",
      title: "除濕機召回公告啟事",
      imgUrl: "assets/imgs/img_noimg.svg",
      link: "",
      createTime: "2019-06-01"
    },
    {
      type: "東元活動",
      title: "[瘋設計] 還在跟PM2.5纏鬥？看看新商品介紹",
      imgUrl: "https://www.teco.com.tw/assets/img/index-pd-4.jpg",
      link: "https://www.teco.com.tw/products/appliances",
      createTime: "2019-12-01"
    }
  ];*/

  constructor(public navCtrl: NavController, public alertProvider: AlertProvider, public api: Api) {

    this.api.getAdvertisement((data) => {
      console.log(data);
      
      this.advertisement = data;
      console.log(this.advertisement);
    }, () => {
      console.log("data error");
    });
  }

  ionViewWillEnter() {
    console.log("檢查信限額度");
    this.creditData = JSON.parse(localStorage.getItem('creditData'));
    console.log(this.creditData);
  }

  goDataLink(data) {
    //let index = this.slideIndexProc();
    //let slide = this.slideDatas[index];

    console.log(data.Link);
    if(data.Link){
      //window.open(data.Link, '_system',
      //'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
      cordova.InAppBrowser.open(data.Link, '_system', 'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
    }
  }

  toOrderPage() {
    if (!this.creditData) {
      this.alertProvider.create({
        title: "無法下單",
        message: "沒有信限資料，請洽業務處理。",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
          }
        ]
      });
      return;
    }

    if (this.creditData.REMAINING < this.creditData.PERCENTBASE) {
      this.alertProvider.create({
        title: "無法下單",
        message: "信限餘額過低，請洽業務處理。",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "確認",
          }
        ]
      });
      return;
    }

    this.navCtrl.push(AddressPage, { animate: false });
    console.log("to order");
  }

  toSettingPage() {
    this.navCtrl.push(SettingPage, { animate: false });
    console.log("to message page");
  }

  toMessagePage() {
    this.navCtrl.push(MessagePage, { animate: false });
    console.log("to message page");
  }

  toWarehousePage() {
    console.log("to warehouse page");

    this.alertProvider.create({
      title: "功能開發中",
      message: "敬請期待",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "確認",
        }
      ]
    });
    return;
  }

  toOrderListPage() {
    this.navCtrl.push(OrderListPage, { animate: false });
    console.log("to order list page");
  }

  toProductLink() {
    console.log("to product link");

    //cordova.InAppBrowser.open("https://www.tecohome.com.tw/tw", '_system', 'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');

    this.navCtrl.push(SpecCategoryPage, { animate: false });
  }
}

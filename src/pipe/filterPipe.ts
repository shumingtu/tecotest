import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {
    transform(list: any[], searchText: string): any {
        if (searchText == null) {
            return list;
        }

        return list.filter((item) => {
            if (item.PRODUCTCODE.indexOf(searchText.toUpperCase()) !== -1 ) {
                return true;
            }
        });
    }
}
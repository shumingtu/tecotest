import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler, NavController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { CalendarModule } from "ion2-calendar";
import { FCM } from '@ionic-native/fcm';
import { MyApp } from './app.component';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { FilterPipe } from '../pipe/filterPipe';
import { MomentPipe } from '../pipe/momentPipe';

import { AlertProvider } from '../providers/alertProvider';
import { SpinnerProvider } from '../providers/spinnerProvider';
import { UserProvider } from '../providers/userProvider';

import { Api } from '../services/api';
import { CordovaService } from '../services/cordovaService';
import { DBManager } from '../services/dbManager';
import { CartManager } from '../services/cartManager';

import { OrderListDao } from '../services/dao/orderListDao';
import { MessagesDao } from '../services/dao/messagesDao';


import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { CategoryPage } from '../pages/category/category';
//import { TabsPage } from '../pages/tabs/tabs';
import { AddressPage } from '../pages/address/address';
import { AcPage } from '../pages/ac/ac';
import { CartPage } from '../pages/cart/cart';
import { ConfirmPage } from '../pages/confirm/confirm';
import { OrderListPage } from '../pages/orderList/orderList';
import { SettingPage } from '../pages/setting/setting';
import { MessagePage } from '../pages/message/message';
import { SpecCategoryPage } from '../pages/specCategory/specCategory';
import { SpecAcPage } from '../pages/specAc/specAc';

import { DateModalPage } from '../pages/dateModal/dateModal';
import { MessageModalPage } from '../pages/messageModal/messageModal';
import { ProductDetailModalPage } from '../pages/productDetailModal/productDetailModal';
import { SpecDetailModalPage } from '../pages/specDetailModal/specDetailModal';
import { OrderDetailModalPage } from '../pages/orderDetailModal/orderDetailModal';
import { SetPasswordModalPage } from '../pages/setPasswordModal/setPasswordModal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    CategoryPage,
    //TabsPage,
    AddressPage,
    AcPage,
    CartPage,
    ConfirmPage,
    OrderListPage,
    SettingPage,
    MessagePage,
    SpecCategoryPage,
    SpecAcPage,
    DateModalPage,
    MessageModalPage,
    ProductDetailModalPage,
    SpecDetailModalPage,
    OrderDetailModalPage,
    SetPasswordModalPage,

    FilterPipe,
    MomentPipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    CalendarModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    CategoryPage,
    AddressPage,
    AcPage,
    //TabsPage,
    CartPage,
    ConfirmPage,
    OrderListPage,
    SettingPage,
    MessagePage,
    SpecCategoryPage,
    SpecAcPage,
    MessageModalPage,
    ProductDetailModalPage,
    SpecDetailModalPage,
    OrderDetailModalPage,
    SetPasswordModalPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    FCM,
    AppVersion,
    Device,
    UniqueDeviceID,
    AlertProvider,
    SpinnerProvider,
    UserProvider,
    Api,
    InAppBrowser,
    CordovaService,
    OrderListDao,
    MessagesDao,
    DBManager,
    CartManager,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

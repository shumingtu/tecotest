import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CordovaService } from '../services/cordovaService';
import { DBManager } from '../services/dbManager';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = LoginPage;

    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public cordovaService: CordovaService,
        public dbManager: DBManager) {
        platform.ready().then(() => {
            cordovaService.init()
                .then(() => dbManager.init())
                .then(() => cordovaService.loadDeviceInfo())
                .then(() => {
                    statusBar.styleDefault();
                    splashScreen.hide();
                    cordovaService.fcmConfig();
                })
        });
    }
}

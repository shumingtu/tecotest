import Dexie from 'dexie';

export class DexieProvider {
    static db: any;

    static init () {
        this.db = new Dexie('TecoDB');
        this.db.version(1).stores({
            orderList: 'id',
            messages: 'messageId',
        });
    }

    static getDB() {
        if (!this.db) {
            this.init();
        }
        return this.db;
    }
}
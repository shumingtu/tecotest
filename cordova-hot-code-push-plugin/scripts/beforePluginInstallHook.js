// fortify issue mark all
// /**
// Hook is executed when plugin is added to the project.
// It will check all necessary module dependencies and install the missing ones locally.
// Also, it will suggest to user to install CLI client for that plugin.
// It can be found in https://github.com/nordnet/cordova-hot-code-push-cli
// */
// var whitelist = {
//     cordovaHcp: 'cordova-hcp',
//     cwdPath: './plugins/',
//     install: 'install',
//     prod: '--production',
// }

// var whileList = {
//     "a": "a", "b": "b", "c": "c", "d": "d", "e": "e",
//     "f": "f", "g": "g", "h": "h", "i": "i", "j": "j",
//     "k": "k", "l": "l", "m": "m", "n": "n", "o": "o",
//     "p": "p", "q": "q", "r": "r", "s": "s", "t": "t",
//     "u": "u", "v": "v", "w": "w", "x": "x", "y": "y",
//     "z": "z", "A": "A", "B": "B", "C": "C", "D": "D",
//     "E": "E", "F": "F", "G": "G", "H": "H", "I": "I",
//     "J": "J", "K": "K", "L": "L", "M": "M", "N": "N",
//     "O": "O", "P": "P", "Q": "Q", "R": "R", "S": "S",
//     "T": "T", "U": "U", "V": "V", "W": "W", "X": "X",
//     "Y": "Y", "Z": "Z", "0": "0", "1": "1", "2": "2",
//     "3": "3", "4": "4", "5": "5", "6": "6", "7": "7",
//     "8": "8", "9": "9", "-": "-", ".": ".", "/": "/",
// };

// function validCommandVal(val){
//     var length = val.length;
//     var resVal = "";

//     for(var idx = 0; idx < length; idx++){
//         resVal += whileList[val.charAt(idx)];
//     }

//     return resVal;
// }

// var path = require('path');
// var fs = require('fs');
// var spawnSync = require('child_process').spawnSync;
// var pluginNpmDependencies = require('../package.json').dependencies;
// var INSTALLATION_FLAG_FILE_NAME = '.npmInstalled';

// // region CLI specific

// /**
//  * Check if cordova-hcp utility is installed. If not - suggest user to install it.
//  */
// function checkCliDependency(ctx) {
//   var val = validCommandVal(ctx.opts.plugin.id);
//   var hcp = checkFromWhiteList('cordovaHcp');
//   var path = checkFromWhiteList('cwdPath');
//   console.log('---------CHCP-------------');
//   console.log('cwd:' + path + val);
//   if(hcp && path){
//     var result = spawnSync(hcp, [], { cwd: path + val });
//     if (!result.error) {
//       return;
//     }

//     suggestCliInstallation();
//   }    
// }

// function checkFromWhiteList(str){
//     return whitelist[str];
// }

// /**
//  * Show message, that developer should install CLI client for the plugin, so it would be easier to use.
//  */
// function suggestCliInstallation() {
//   console.log('---------CHCP-------------');
//   console.log('To make the development process easier for you - we developed a CLI client for our plugin.');
//   console.log('To install it, please, use command:');
//   console.log('npm install -g cordova-hot-code-push-cli');
//   console.log('For more information please visit https://github.com/nordnet/cordova-hot-code-push-cli');
//   console.log('--------------------------');
// }

// // endregion

// // region mark that we installed npm packages
// /**
//  * Check if we already executed this hook.
//  *
//  * @param {Object} ctx - cordova context
//  * @return {Boolean} true if already executed; otherwise - false
//  */
// function isInstallationAlreadyPerformed(ctx) {
//   var pathToInstallFlag = path.join(ctx.opts.projectRoot, 'plugins', ctx.opts.plugin.id, INSTALLATION_FLAG_FILE_NAME);
//   try {
//     fs.accessSync(pathToInstallFlag, fs.F_OK);
//     return true;
//   } catch (err) {
//     return false;
//   }
// }

// /**
//  * Create empty file - indicator, that we tried to install dependency modules after installation.
//  * We have to do that, or this hook is gonna be called on any plugin installation.
//  */
// function createPluginInstalledFlag(ctx) {
//   var pathToInstallFlag = path.join(ctx.opts.projectRoot, 'plugins', ctx.opts.plugin.id, INSTALLATION_FLAG_FILE_NAME);

//   fs.closeSync(fs.openSync(pathToInstallFlag, 'w'));
// }
// // endregion

// module.exports = function(ctx) {
//   var val = validCommandVal(ctx.opts.plugin.id);

//   if (isInstallationAlreadyPerformed(ctx)) {
//     return;
//   }

//   console.log('Installing dependency packages: ');
//   console.log(JSON.stringify(pluginNpmDependencies, null, 2));

//   var npm = (process.platform === "win32" ? "npm.cmd" : "npm");
//   var install = checkFromWhiteList('install');
//   var prod = checkFromWhiteList('prod');
//   var path = checkFromWhiteList('cwdPath');

//   if( install && prod && path){
//     var result = spawnSync(npm, [install, prod], { cwd: path + val });
//     if (result.error) {
//       throw result.error;
//     }
  
//     createPluginInstalledFlag(ctx);
  
//     console.log('Checking cordova-hcp CLI client...');
  
//     checkCliDependency(ctx);
//   }  
// };

package com.nordnetab.chcp.main.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
/**
 * Created by Nikolay Demyankov on 03.06.16.
 * <p/>
 * Helper class to work with URLConnection
 */
public class URLConnectionHelper {

    // connection timeout in milliseconds
    private static final int CONNECTION_TIMEOUT = 30000;

    // data read timeout in milliseconds
    private static final int READ_TIMEOUT = 30000;

    /**
     * Create URLConnection instance.
     *
     * @param url            to what url
     * @param requestHeaders additional request headers
     * @return connection instance
     * @throws IOException when url is invalid or failed to establish connection
     */
    public static URLConnection createConnectionToURL(final String url, final Map<String, String> requestHeaders) throws IOException {
        final URL connectionURL = new URL(toUtf8String(url));
        if (connectionURL == null) {
            throw new IOException("Invalid url format: " + url);
        }

        final URLConnection urlConnection = connectionURL.openConnection();
        urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
        urlConnection.setReadTimeout(READ_TIMEOUT);

        if (requestHeaders != null) {
            for (final Map.Entry<String, String> entry : requestHeaders.entrySet()) {
                urlConnection.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        return urlConnection;
    }


    public static String toUtf8String(String s) {
        String [] path = s.split("/");
        String [] fileName = path[path.length - 1].split("\\.");

        String result = null;
        try {
            result = URLEncoder.encode(fileName[0],"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return s.replace(fileName[0], result);
    }
}

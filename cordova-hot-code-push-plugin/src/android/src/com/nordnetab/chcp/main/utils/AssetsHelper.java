package com.nordnetab.chcp.main.utils;

import android.content.Context;
import android.util.Log;

import com.nordnetab.chcp.main.events.AssetsInstallationErrorEvent;
import com.nordnetab.chcp.main.events.AssetsInstalledEvent;
import com.nordnetab.chcp.main.events.BeforeAssetsInstalledEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by Nikolay Demyankov on 21.07.15.
 * <p/>
 * Utility class to copy files from assets folder into the external storage.
 */
public class AssetsHelper {

    private static boolean isWorking;

    private AssetsHelper() {
    }

    /**
     * Copy files from the assets folder into the specific folder on the external storage.
     * Method runs asynchronously. Results are dispatched through events.
     *
     * @param applicationContext current application context
     * @param fromDirectory      which directory in assets we want to copy
     * @param toDirectory        absolute path to the destination folder on the external storage
     * @see AssetsInstallationErrorEvent
     * @see AssetsInstalledEvent
     * @see EventBus
     */
    public static void copyAssetDirectoryToAppDirectory(final Context applicationContext, final String fromDirectory, final String toDirectory) {
        if (isWorking) {
            return;
        }
        isWorking = true;

        // notify, that we are starting assets installation
        EventBus.getDefault().post(new BeforeAssetsInstalledEvent());

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    execute(applicationContext, fromDirectory, toDirectory);
                    EventBus.getDefault().post(new AssetsInstalledEvent());
                } catch (IOException e) {
                    e.printStackTrace();
                    EventBus.getDefault().post(new AssetsInstallationErrorEvent());
                } finally {
                    isWorking = false;
                }
            }
        }).start();
    }

    private static void execute(Context applicationContext, String fromDirectory, String toDirectory) throws IOException {
        // recreate cache folder
        FilesUtility.delete(toDirectory);
        FilesUtility.ensureDirectoryExists(toDirectory);

        final String assetsDir = "assets/" + fromDirectory;
        copyAssets(applicationContext.getApplicationInfo().sourceDir, assetsDir, toDirectory);
    }

    private static void copyAssets(final String appJarPath, final String assetsDir, final String toDirectory) throws IOException {
        JarFile jarFile = null;

        try{
            jarFile = new JarFile(appJarPath);
            final int prefixLength = assetsDir.length();
            final Enumeration<JarEntry> filesEnumeration = jarFile.entries();

            while (filesEnumeration.hasMoreElements()) {
                final JarEntry fileJarEntry = filesEnumeration.nextElement();
                final String name = fileJarEntry.getName();
                if (!fileJarEntry.isDirectory() && name.startsWith(assetsDir)) {
                    final String destinationFileAbsolutePath = Paths.get(toDirectory, name.substring(prefixLength));

                    copyFile(jarFile.getInputStream(fileJarEntry), destinationFileAbsolutePath);
                }
            }
        } catch (IOException e) {
            Log.d("exception", e.getMessage());
            e.printStackTrace();
        } finally {
            if(jarFile != null){
                safeClose(jarFile);
            }
        }
    }

    public static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.d("exception", e.getMessage());
            }
        }
    }

    /**
     * Copies asset file to destination path
     */
    private static void copyFile(final InputStream in, final String destinationFilePath) throws IOException {
//        FilesUtility.ensureDirectoryExists(new File(destinationFilePath).getParent());
//        OutputStream out = new FileOutputStream(destinationFilePath);
        final String filterPath = validFilePath(destinationFilePath);
        FilesUtility.ensureDirectoryExists(new File(filterPath).getParent());
        OutputStream out = null;

        try{
            out = new FileOutputStream(filterPath);
            // Transfer bytes from in to out
            byte[] buf = new byte[8192];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        } catch (IOException e){
            Log.d("exception", e.getMessage());
            e.printStackTrace();
        } finally {
            if(out != null){
                safeClose(out);
            }
        }
    }

    private static String validFilePath(String filepath){
        String temp = "";

        for(int idx = 0; idx < filepath.length(); idx++){
            Character curChar = null;
            Character nextChar = null;
            try{
                curChar = filepath.charAt(idx);
                nextChar = filepath.charAt(idx + 1);
            } catch(Exception e){

            }

            if(pathCharWhileList.get(filepath.charAt(idx)+ "") != null && curChar == '\\'){
                String sysFileSeparator = File.separator;
                if(null != sysFileSeparator && sysFileSeparator.equals(curChar.toString())){
                    temp += pathCharWhileList.get(filepath.charAt(idx) + "");
                }
            } else if(pathCharWhileList.get(filepath.charAt(idx) + "") != null && curChar != '.'){
                temp += pathCharWhileList.get(filepath.charAt(idx) + "");
            } else if(pathCharWhileList.get(filepath.charAt(idx) + "") != null && curChar == '.' && nextChar != '.'){
                temp += pathCharWhileList.get(filepath.charAt(idx) + "");
            }
        }

        return temp;
    }

    private final static Map<String, String> pathCharWhileList = new HashMap<String, String>();
    static {
        pathCharWhileList.put("a", "a");
        pathCharWhileList.put("b", "b");
        pathCharWhileList.put("c", "c");
        pathCharWhileList.put("d", "d");
        pathCharWhileList.put("e", "e");
        pathCharWhileList.put("f", "f");
        pathCharWhileList.put("g", "g");
        pathCharWhileList.put("h", "h");
        pathCharWhileList.put("i", "i");
        pathCharWhileList.put("j", "j");
        pathCharWhileList.put("k", "k");
        pathCharWhileList.put("l", "l");
        pathCharWhileList.put("m", "m");
        pathCharWhileList.put("n", "n");
        pathCharWhileList.put("o", "o");
        pathCharWhileList.put("p", "p");
        pathCharWhileList.put("q", "q");
        pathCharWhileList.put("r", "r");
        pathCharWhileList.put("s", "s");
        pathCharWhileList.put("t", "t");
        pathCharWhileList.put("u", "u");
        pathCharWhileList.put("v", "v");
        pathCharWhileList.put("w", "w");
        pathCharWhileList.put("x", "x");
        pathCharWhileList.put("y", "y");
        pathCharWhileList.put("z", "z");
        pathCharWhileList.put("A", "A");
        pathCharWhileList.put("B", "B");
        pathCharWhileList.put("C", "C");
        pathCharWhileList.put("D", "D");
        pathCharWhileList.put("E", "E");
        pathCharWhileList.put("F", "F");
        pathCharWhileList.put("G", "G");
        pathCharWhileList.put("H", "H");
        pathCharWhileList.put("I", "I");
        pathCharWhileList.put("J", "J");
        pathCharWhileList.put("K", "K");
        pathCharWhileList.put("L", "L");
        pathCharWhileList.put("M", "M");
        pathCharWhileList.put("N", "N");
        pathCharWhileList.put("O", "O");
        pathCharWhileList.put("P", "P");
        pathCharWhileList.put("Q", "Q");
        pathCharWhileList.put("R", "R");
        pathCharWhileList.put("S", "S");
        pathCharWhileList.put("T", "T");
        pathCharWhileList.put("U", "U");
        pathCharWhileList.put("V", "V");
        pathCharWhileList.put("W", "W");
        pathCharWhileList.put("X", "X");
        pathCharWhileList.put("Y", "Y");
        pathCharWhileList.put("Z", "Z");
        pathCharWhileList.put("-", "-");
        pathCharWhileList.put("_", "_");
        pathCharWhileList.put("0", "0");
        pathCharWhileList.put("1", "1");
        pathCharWhileList.put("2", "2");
        pathCharWhileList.put("3", "3");
        pathCharWhileList.put("4", "4");
        pathCharWhileList.put("5", "5");
        pathCharWhileList.put("6", "6");
        pathCharWhileList.put("7", "7");
        pathCharWhileList.put("8", "8");
        pathCharWhileList.put("9", "9");
        pathCharWhileList.put("/", "/");
        pathCharWhileList.put(".", ".");
        pathCharWhileList.put(" ", " ");
    }

}